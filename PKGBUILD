# Maintainer: Mark Wagie <mark@manjaro.org>
# Contributor: Jonathon Fernyhough

# Arch credits:
# Contributor: Maxime Gauduin <alucryd@archlinux.org>
# Contributor: Marc Schulte <bomba@nerdstube.de>

pkgbase=tlp
pkgname=('tlp' 'tlp-rdw')
pkgver=1.6.1
pkgrel=1
arch=('any')
url="https://linrunner.de/tlp"
license=('GPL-2.0-or-later')
makedepends=('git')
checkdepends=('appstream')
_commit=40c44d108182fd57c2ff44d685a8f908a9c782d3  # tags/1.6.1^0
source=("git+https://github.com/linrunner/TLP.git#commit=${_commit}?signed")
sha256sums=('c372994d55ce5eb0fdffcf14b93be924f98e5d193dcfe72be1d82eab6bb12339')
validpgpkeys=('2F4139F5BB74D986D6BA54277B36F42FC064A73B') # Thomas Koch <linrunner@gmx.net>

pkgver() {
  cd "$srcdir/TLP"
  git describe --tags | sed 's/-/+/g'
}

check() {
  cd "$srcdir/TLP"
  appstreamcli validate --no-net de.linrunner.tlp.metainfo.xml
}

package_tlp() {
  pkgdesc="Linux Advanced Power Management"
  depends=(
    'hdparm'
    'iw'
    'pciutils'
    'perl'
    'rfkill'
    'usbutils'
    'util-linux'
  )
  optdepends=(
    'ethtool: Disable Wake On Lan'
    'smartmontools: Display S.M.A.R.T. data in tlp-stat'
    'tp_smapi: Older ThinkPad battery functions (before Sandy Bridge)'
  )
  conflicts=(
    'laptop-mode-tools'
    'pm-utils'
  )
  backup=('etc/tlp.conf')
  install='tlp.install'

  export TLP_NO_INIT=1
  export TLP_SBIN=/usr/bin
  export TLP_SDSL=/usr/lib/systemd/system-sleep
  export TLP_SYSD=/usr/lib/systemd/system
  export TLP_ULIB=/usr/lib/udev
  export TLP_WITH_ELOGIND=0
  export TLP_WITH_SYSTEMD=1

  make DESTDIR="${pkgdir}" -C TLP install-tlp install-man-tlp
}

package_tlp-rdw() {
  pkgdesc="Linux Advanced Power Management - Radio Device Wizard"
  depends=(
    'networkmanager'
    'tlp'
  )
  install='tlp-rdw.install'

  make DESTDIR="${pkgdir}" -C TLP install-rdw install-man-rdw
}

# vim: ts=2 sw=2 et:
